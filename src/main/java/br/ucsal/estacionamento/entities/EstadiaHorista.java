package br.ucsal.estacionamento.entities;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "EstadiaHorista")
@PrimaryKeyJoinColumn(name="id_estadia")
public class EstadiaHorista extends Estadia {

	@Column(name = "placa")
    private String placa;

	public EstadiaHorista(){
		
	}
	
	public EstadiaHorista(String placa) {
		super(Timestamp.valueOf(LocalDateTime.now()));
		this.placa = placa;

	}
	public EstadiaHorista(String placa, Timestamp entrada, Timestamp saida) {
		super(entrada, saida);
		this.placa = placa;
	}
}