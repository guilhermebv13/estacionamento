package br.ucsal.estacionamento.entities;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "estadia_mensalista")
@PrimaryKeyJoinColumn(name="id_estadia")
public class EstadiaMensalista extends Estadia{

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    private Cliente cliente;

    public EstadiaMensalista() {
		super();
	}

    public EstadiaMensalista(Cliente cliente, Timestamp entrada, Timestamp saida){
    	super(entrada, saida);
    	this.cliente = cliente;
    }
    
    public EstadiaMensalista(Cliente cliente){
    	super(Timestamp.valueOf(LocalDateTime.now()));
    	this.cliente = cliente;
    }
}
