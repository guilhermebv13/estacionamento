package br.ucsal.estacionamento.entities;

import java.sql.Timestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "estadia")
@Inheritance(strategy = InheritanceType.JOINED)
public class Estadia {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id_estadia")
    private Long idEstadia;

    @Column(name = "entrada", nullable=false)
    private Timestamp entrada;

    @Column(name = "saida")
    private Timestamp saida;
    

	public Estadia() {
	}

	public Estadia(Timestamp entrada){
		this.entrada = entrada;
	}
	
    public Estadia(Timestamp entrada, Timestamp saida){
        this.entrada = entrada;
        this.saida = saida;
    }

}