package br.ucsal.estacionamento.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "vaga")
public class Vaga {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vaga")
    private Long idVaga;

    @Column(name = "disponivel")
    private boolean disponivel;

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    private Cliente cliente;

    @Column(name = "numero", unique = true)
    private int numero;

    public Vaga() {
    	
    }

	public Vaga(boolean disponivel, Cliente cliente, int numero) {
		super();
		this.disponivel = disponivel;
		this.cliente = cliente;
		this.numero = numero;
	}

    
}
