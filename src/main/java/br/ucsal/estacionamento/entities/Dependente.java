package br.ucsal.estacionamento.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "Dependente")
public class Dependente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_dependente")
    private Long idDependente;

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    private Cliente cliente;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf")
    private String cpf;

    public Dependente() {
    	
    }
    
	public Dependente(Cliente cliente, String nome, String cpf) {
		super();
		this.cliente = cliente;
		this.nome = nome;
		this.cpf = cpf;
	}

}