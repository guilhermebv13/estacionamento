package br.ucsal.estacionamento.entities;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "cliente")
public class Cliente {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente")
    private Long idCliente;

    @Column(name = "nome")
    private String nome;

    @Column(name = "email", unique=true)
    private String email;

    @Column(name = "cpf", unique=true)
    private String cpf;

    @ManyToOne
    @JoinColumn(name = "id_endereco", referencedColumnName = "id_endereco")
    private Endereco endereco;

    @Column(name = "telefone", unique=true)
    private String telefone;

    @Column(name = "nascimento")
    private Date nascimento;

    @Column(name = "inadimplente")
    private boolean inadimplente;

    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Dependente> dependentes;

    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Veiculo> veiculos;

    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EstadiaMensalista> estadiasMensalistas;

    
    public Cliente() {
    	
    }
    
    public Cliente(String nome, String email, String cpf, String telefone, Date nascimento, Endereco endereco) {
    	this.nome = nome;
    	this.email = email;
    	this.cpf = cpf;
    	this.telefone= telefone;
    	this.nascimento = nascimento;
    	this.endereco = endereco;
    	this.inadimplente = false;
    	this.dependentes = Collections.checkedList(Collections.emptyList(), Dependente.class);
    	this.veiculos = Collections.checkedList(Collections.emptyList(), Veiculo.class);
    	this.estadiasMensalistas = Collections.checkedList(Collections.emptyList(), EstadiaMensalista.class);
   	}


	public Long getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public Endereco getEndereco() {
		return endereco;
	}


	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public Date getNascimento() {
		return nascimento;
	}


	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}


	public boolean isInadimplente() {
		return inadimplente;
	}


	public void setInadimplente(boolean inadimplente) {
		this.inadimplente = inadimplente;
	}


	public List<Dependente> getDependentes() {
		return dependentes;
	}


	public void setDependentes(List<Dependente> dependentes) {
		this.dependentes = dependentes;
	}


	public List<Veiculo> getVeiculos() {
		return veiculos;
	}


	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}


	public List<EstadiaMensalista> getEstadiasMensalistas() {
		return estadiasMensalistas;
	}


	public void setEstadiasMensalistas(List<EstadiaMensalista> estadiasMensalistas) {
		this.estadiasMensalistas = estadiasMensalistas;
	}
	
	@Override
	public String toString() {
		return this.nascimento.toString();
	}
    
    
}

