package br.ucsal.estacionamento.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import br.ucsal.estacionamento.entities.Cliente;
import br.ucsal.estacionamento.entities.Dependente;
import br.ucsal.estacionamento.entities.Endereco;
import br.ucsal.estacionamento.entities.EstadiaHorista;
import br.ucsal.estacionamento.entities.EstadiaMensalista;
import br.ucsal.estacionamento.entities.Mensalidade;
import br.ucsal.estacionamento.entities.Tarifa;
import br.ucsal.estacionamento.entities.Vaga;
import br.ucsal.estacionamento.entities.Veiculo;
import br.ucsal.estacionamento.repository.ClienteRepository;
import br.ucsal.estacionamento.repository.DependenteRepository;
import br.ucsal.estacionamento.repository.EnderecoRepository;
import br.ucsal.estacionamento.repository.EstadiaHoristaRepository;
import br.ucsal.estacionamento.repository.EstadiaMensalistaRepository;
import br.ucsal.estacionamento.repository.MensalidadeRepository;
import br.ucsal.estacionamento.repository.TarifaRepository;
import br.ucsal.estacionamento.repository.VagaRepository;
import br.ucsal.estacionamento.repository.VeiculoRepository;
import jakarta.persistence.EntityManager;
import net.datafaker.Faker;

@Controller
public class EstacionamentoController {

	@Autowired
	EntityManager em;
	
	@Autowired	
	ClienteRepository clienteRepo;

	@Autowired
	VeiculoRepository veiculoRepo;
	
	@Autowired
	EnderecoRepository enderecoRepo;

	@Autowired
	VagaRepository vagaRepo;
	
	@Autowired
	DependenteRepository dependenteRepo;
	
	@Autowired
	TarifaRepository tarifaRepo;
	
	@Autowired
	MensalidadeRepository mensalidadeRepo;
	
	@Autowired
	EstadiaMensalistaRepository estadiaMensalistaRepo;
	
	@Autowired
	EstadiaHoristaRepository estadiaHoristaRepo;
	
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Faker faker = new Faker();

	public record Cadastro(String nome, String email, String cpf, String telefone, String nascimento, String modelo, String placa, String bairro, String logradouro, Integer numeroEndereco, String cep,
						Integer numeroVaga, String nomeDependente, String cpfDependente) {
	}
	
	@PostMapping("/cadastrar")
	public String cadastrar(@ModelAttribute("cadastro") Cadastro cadastro) throws ParseException {
		
		Endereco endereco = new Endereco(cadastro.bairro, cadastro.logradouro, cadastro.numeroEndereco, cadastro.cep);
		endereco = enderecoRepo.save(endereco);
		
		Cliente cliente = new Cliente(cadastro.nome, cadastro.email, cadastro.cpf, cadastro.telefone, formatter.parse(cadastro.nascimento), endereco);
		cliente = clienteRepo.save(cliente);
		
		Veiculo veiculo = new Veiculo(cadastro.modelo, cadastro.placa, cliente);
		veiculo = veiculoRepo.save(veiculo);
		
		Vaga vaga = new Vaga(true, cliente, cadastro.numeroVaga);
		vaga = vagaRepo.save(vaga);
		
		Dependente dependente = new Dependente(cliente,cadastro.nome,cadastro.cpf);
		dependenteRepo.save(dependente);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date endOfMonth = calendar.getTime();
		
		Tarifa tarifa = tarifaRepo.findByDescricao("mensal").get();
		Mensalidade mensalidade = new Mensalidade(cliente, tarifa.getValor(), endOfMonth, false);
		mensalidade = mensalidadeRepo.save(mensalidade);
		
		return "redirect:/estadia";
	}
	
	public record CadastroEstadiaMensalista(String cpf) {
		
	}
	
	@PostMapping("/cadastrarEstadiaMensalista")
	public String cadastrarEstadia(CadastroEstadiaMensalista cadastro) {
		Cliente cliente = clienteRepo.findByCpf(cadastro.cpf).get();
		EstadiaMensalista estadia = new EstadiaMensalista(cliente);
		estadiaMensalistaRepo.save(estadia);
		return "index.html";
	}
	
	public record CadastroEstadiaHorista(String placa) {
		
	}
	
	@PostMapping("/cadastrarEstadiaHorista")
	public String cadastrarEstadiaHorista(CadastroEstadiaHorista cadastro) {
		EstadiaHorista estadia = new EstadiaHorista(cadastro.placa);
		estadiaHoristaRepo.save(estadia);
		return "index.html";
	}
	
	@GetMapping("/estadia")
	public String estadia() {
		return "estadia.html";
	}
	
	@GetMapping("/teste")
	public String teste() throws ParseException {
		return faker.animal().scientificName();
	}	
	
	@GetMapping("/carga")
	public String carga() throws ParseException, InterruptedException {
		ForkJoinPool threadPool = new ForkJoinPool(10);
//		List<Cliente> lClientes = clienteRepo.findSemEstadia();
//
//		for (Cliente cliente : lClientes) {
//			threadPool.execute(()-> {
				popular2(threadPool);
//			});
//		}
//		
//		threadPool.shutdown();
//		threadPool.awaitTermination(30, TimeUnit.MINUTES);

		return "redirect:/";
		
	}
	
	
	private void popular1() throws ParseException {
		Endereco endereco = new Endereco(faker.pokemon().name(), faker.naruto().character(), faker.number().numberBetween(1, 3000), faker.examplify("40283-220"));
		endereco = enderecoRepo.save(endereco);
		
		Cliente cliente = new Cliente(faker.name().fullName(), faker.internet().emailAddress(), faker.cpf().valid(true), faker.phoneNumber().cellPhone(), Date.from(faker.date().birthdayLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant()), endereco);
		clienteRepo.save(cliente);
	}
	
	private void popular2(ForkJoinPool threadPool) throws ParseException {
		List<Cliente> lClientes = clienteRepo.findSemVeiculo();
		Random random = new Random();
		
		lClientes.forEach(cliente -> {
			threadPool.execute(()-> {
				Veiculo v = new Veiculo(faker.vehicle().model(), faker.vehicle().licensePlate(), lClientes.get(random.nextInt(lClientes.size())));
				veiculoRepo.save(v);
				
				Dependente dep = new Dependente(lClientes.get(random.nextInt(lClientes.size())), faker.name().fullName(), faker.cpf().valid(true));
				dependenteRepo.save(dep);
				
				Vaga vg = new Vaga(false, cliente, faker.number().positive());
				vagaRepo.save(vg);
			});
		});
	}
	
	private void popular3(Cliente cliente) {
		int numEstadias = new Random().nextInt(1,11);
		int i = 0;
		while(i < numEstadias) {
			Timestamp saida = Timestamp.valueOf(LocalDateTime.of(faker.date().birthdayLocalDate(0, 24), LocalTime.ofSecondOfDay(faker.number().numberBetween(0, 24*60*60))));
			saida.setHours(faker.number().numberBetween(1,23));
			saida.setMinutes(faker.number().numberBetween(0, 59));
			saida.setSeconds(faker.number().numberBetween(0, 59));

			Timestamp entrada = new Timestamp(saida.getTime());
			entrada.setHours(faker.number().numberBetween(0, saida.getHours()-1));
			entrada.setMinutes(faker.number().numberBetween(0, saida.getMinutes()));
			entrada.setSeconds(faker.number().numberBetween(0, saida.getSeconds()));

			EstadiaMensalista m = new EstadiaMensalista(cliente, entrada, saida);
			estadiaMensalistaRepo.save(m);
			if(i%3==0)
			estadiaMensalistaRepo.flush();
			i++;
		}
//		saida = Timestamp.valueOf(LocalDateTime.of(faker.date().birthdayLocalDate(0, 100), LocalTime.ofSecondOfDay(faker.number().numberBetween(0, 24*60*60))));
//		saida.setHours(faker.number().numberBetween(1,23));
//		saida.setMinutes(faker.number().numberBetween(0, 59));
//		saida.setSeconds(faker.number().numberBetween(0, 59));
//		
//		entrada = new Timestamp(saida.getTime());
//		entrada.setHours(faker.number().numberBetween(0, saida.getHours()-1));
//		entrada.setMinutes(faker.number().numberBetween(0, saida.getMinutes()));
//		entrada.setSeconds(faker.number().numberBetween(0, saida.getSeconds()));
//		
//		EstadiaHorista h = new EstadiaHorista(faker.vehicle().licensePlate(),entrada, saida); 
//		estadiaHoristaRepo.save(h); 
//		estadiaHoristaRepo.flush();
	}
	
	public void popular4(Cliente cliente) {
		BigDecimal valorMensal = tarifaRepo.findByDescricao("mensal").get().getValor();
		int numVagas = vagaRepo.findQtdVagasPorCliente(cliente);
		
		Timestamp entrada = estadiaMensalistaRepo.findPrimeiraEntradaByCliente(cliente);
		double valor = (numVagas > 1 )? (valorMensal.doubleValue() * numVagas)/1.3 : valorMensal.doubleValue();
		
		Date data = new Date(entrada.getYear(),entrada.getMonth(),9);
		int anoAtual = new Date().getYear();
		
		while(data.getYear()<anoAtual) {
			Mensalidade m = new Mensalidade(cliente, BigDecimal.valueOf(valor), data, true);
			mensalidadeRepo.save(m);
			mensalidadeRepo.flush();
			if(data.getMonth()==11) {
				data.setMonth(0);
				data.setYear(data.getYear()+1);
			}else {
				data.setMonth(data.getMonth()+1);
			}
		}
		
	}
	
	public void ajeitarcpf() {
		
	}
}