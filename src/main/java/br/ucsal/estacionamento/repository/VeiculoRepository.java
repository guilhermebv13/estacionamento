package br.ucsal.estacionamento.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Cliente;
import br.ucsal.estacionamento.entities.Veiculo;

@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {
	Optional<Veiculo> findByCliente(Cliente id);
}
