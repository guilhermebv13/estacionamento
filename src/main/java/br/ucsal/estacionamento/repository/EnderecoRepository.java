package br.ucsal.estacionamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Endereco;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco,Long> {

}
