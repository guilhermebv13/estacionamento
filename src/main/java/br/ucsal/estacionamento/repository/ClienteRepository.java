package br.ucsal.estacionamento.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Long> {
	
	Optional<Cliente> findByCpf(String cpf);
	
	@Query("SELECT c FROM Cliente c LEFT JOIN c.veiculos v WHERE v IS NULL")
    List<Cliente> findSemVeiculo();
	
//	@Query("SELECT c FROM Cliente c WHERE c.idCliente NOT IN (SELECT em.cliente.idCliente FROM EstadiaMensalista em)")
	@Query("SELECT c FROM Cliente c LEFT JOIN c.estadiasMensalistas em WHERE em IS NULL")
	List<Cliente> findSemEstadia();

}
