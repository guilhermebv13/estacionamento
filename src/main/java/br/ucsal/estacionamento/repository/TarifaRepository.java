package br.ucsal.estacionamento.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Tarifa;

@Repository
public interface TarifaRepository extends JpaRepository<Tarifa,Long> {

	Optional<Tarifa> findByDescricao(String descricao);
}
