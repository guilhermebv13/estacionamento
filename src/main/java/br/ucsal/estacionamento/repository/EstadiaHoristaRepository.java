package br.ucsal.estacionamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.EstadiaHorista;

@Repository
public interface EstadiaHoristaRepository extends JpaRepository<EstadiaHorista, Long> {

}
