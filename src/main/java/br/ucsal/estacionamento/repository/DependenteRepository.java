package br.ucsal.estacionamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Dependente;

@Repository
public interface DependenteRepository extends JpaRepository<Dependente,Long> {

}
