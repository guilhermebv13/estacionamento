package br.ucsal.estacionamento.repository;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Cliente;
import br.ucsal.estacionamento.entities.EstadiaMensalista;

@Repository
public interface EstadiaMensalistaRepository extends JpaRepository<EstadiaMensalista, Long> {

	@Query("SELECT MIN(e.entrada) FROM EstadiaMensalista e WHERE e.cliente = :cliente")
	Timestamp findPrimeiraEntradaByCliente(Cliente cliente);
	
}
