package br.ucsal.estacionamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Cliente;
import br.ucsal.estacionamento.entities.Vaga;

@Repository
public interface VagaRepository extends JpaRepository<Vaga,Long>{

	@Query("SELECT COUNT(v.cliente) FROM Vaga v WHERE v.cliente = :cliente GROUP BY v.cliente")
	int findQtdVagasPorCliente(Cliente cliente);
	
}