package br.ucsal.estacionamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.estacionamento.entities.Mensalidade;

@Repository
public interface MensalidadeRepository extends JpaRepository<Mensalidade,Long> {

}
